let fs = require('fs')
const { spawn, spawnSync } = require('child_process')
var minimatch = require('minimatch')
const yargs = require('yargs')
const chokidar = require('chokidar')
let http = require('http')

const argv = yargs
    .option('specs', {
        alias: 's',
        description: 'Define specs pattern',
        type: 'string',
        default: 'Integration/**/*',
    })
    .option('parallel', {
        alias: 'p',
        description: 'Define parallel option',
        type: 'boolean',
        default: false,
    })
    .option('buildID', {
        alias: 'id',
        description: 'Define build id option',
        type: 'string',
        default: '',
    })
    .option('prebuild', {
        alias: 'pb',
        description: 'Generate prebuild file',
        type: 'boolean',
        default: false,
    })
    .help()
    .alias('help', 'h').argv

function getFiles(dir, files_) {
    files_ = files_ || []
    var files = fs.readdirSync(dir)
    for (var i in files) {
        var name = dir + '/' + files[i]
        if (fs.statSync(name).isDirectory()) {
            getFiles(name, files_)
        } else {
            const fileName = files[i].split('.')
            name = name.replace(/\.\/src\//gi, '')
            files_.push(name)
        }
    }
    return files_
}

const entries = getFiles('./src/Integration')
let imports = []
let defs = []

let specs = process.env.SPECS || argv.specs

specs = specs.replace(/src\//g, '')

if (!specs) {
    return console.error('missing specs')
}

let args = ['run', 'k6', '--', '--summary-export', 'test.json']
let logInit = ''
let logFinish = ''
let elements = []
imports.push(`import { check, group } from 'k6'`)
imports.push(`import { log } from './common/general'`)

for (let index = 0; index < entries.length; index++) {
    let element = entries[index]
    element = element.split('.ts')[0]
    if (minimatch(element, specs)) {
        elements.push(element)

        logInit = `log('*** Init ${element} ***')`
        logFinish = `log('*** Finish ${element} ***')`

        imports.push(
            `import def${index + 1}, {setup as s${index + 1}, teardown as t${
                index + 1
            }} from './${element}'`,
        )

        const blockTest = `
        __ENV['current_test'] = '${element}'
        try {
            ${logInit}
            group('${element}', () => {
                const ss${index + 1} = s${index + 1}()
                def${index + 1}(ss${index + 1})
                t${index + 1}(ss${index + 1})
            })
            ${logFinish}
        } catch (error) {
            group('${element}', () => {
                group('${element}_test_fail => ' + error, () => {
                    let target: any = {}
                    log('=====${element}===== '+error)
                    target['${element} => ' + error] = () => false
                    check(0, target)
                })
            })
        }        
        `
        if (argv.parallel && argv.buildID.trim() !== '') {
            defs.push(`
                if(test === "${element}"){
                    ${blockTest}
                }`)
        } else {
            defs.push(`${blockTest}`)
        }
    }
}

let impl = `${imports.join('\n')}`

if (argv.parallel && argv.buildID.trim() !== '') {
    imports.push(`import { run } from './lib/process_remote'`)
    impl = `${imports.join('\n')}`

    const system = require('./lib/system')
    var git = require('git-last-commit')

    git.getLastCommit(function (err, commit) {
        if (!commit) {
            commit = {
                hash: process.env['CI_COMMIT_SHA'] || '...',
                subject: process.env['CI_COMMIT_TITLE'] || '...',
                body: process.env['CI_COMMIT_MESSAGE'] || '',
                author: {
                    name: process.env['GITLAB_USER_NAME'] || 'User',
                    email: process.env['GITLAB_USER_EMAIL'] || 'mail@test.com',
                },
                branch: process.env['CI_COMMIT_BRANCH'] || 'develop',
            }
        }

        system.info().then((data) => {
            impl += `
        
export default function() {
    const system = ${JSON.stringify(data)}
    const commit = ${JSON.stringify(commit)}
    const files = ${JSON.stringify(elements)}
    const id = __ENV.buildID || '${argv.buildID}'
    const specPattern = '${specs}'
    run({
        buildID: id,
        commit,
        specs: files,
        system,
        test: runTests,
        specPattern
    })
}

function runTests(test: string) {
    ${defs.join('\n')}
}`

            fs.writeFileSync('src/test.ts', impl, function (err) {
                if (err) return console.log(err)
                console.log('Entry file created')
            })
            spawnSync('webpack', {
                env: {
                    ...process.env,
                    SPECS: specs,
                },
                customFds: [0, 1, 2],
                shell: true,
                stdio: 'inherit',
            })
        })
    })
} else {
    impl += `
    export default function() {
        ${defs.join('\n')}
    }`
}

if (argv.parallel && argv.buildID.trim() !== '') {
    return
}

fs.writeFileSync('src/test.ts', impl, function (err) {
    if (err) return console.log(err)
    console.log('Entry file created')
})

spawnSync('webpack', {
    env: {
        ...process.env,
        SPECS: specs,
    },
    customFds: [0, 1, 2],
    shell: true,
    stdio: 'inherit',
})

if (argv.prebuild) {
    return
}

let options = {
    env: {
        ...process.env,
    },
}

options.customFds = [0, 1, 2]
options.shell = true
options.stdio = 'inherit'
spawn('npm', args, options)
