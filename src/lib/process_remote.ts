import { sleep } from 'k6'
import http from 'k6/http'
import { log } from '../common/general'
import { reportInstance, setupDashboard, setupInstance } from '../fixtures/remote'

interface Run {
    system: any
    commit: any
    specs: any
    test: any
    buildID: string
    specPattern: any
}

interface runInstance {
    groupId: string
    machineId: string
    runId: string
    runUrl: string
    warnings: any
}

interface testInstance {
    claimedInstances: number
    totalInstances: number
    spec: string
    instanceId: string
}

function createRun(run: Run): runInstance {
    let data: any = setupDashboard()
    data.specs = run.specs
    data.commit = {
        ...data.commit,
        sha: run.commit.hash,
        branch: run.commit.branch,
        authorName: run.commit.author.name,
        authorEmail: run.commit.author.email,
        message: run.commit.body || data.commit.message,
    }
    data.platform = {
        ...data.platform,
        ...run.system,
    }
    data.ciBuildId = run.buildID
    data.specPattern = run.specPattern
    const req = http.request('POST', 'http://localhost:1234/runs', JSON.stringify(data), {
        headers: {
            'Content-Type': 'application/json',
            'x-cypress-version': '6.7.1',
        },
    })
    const result: any = req.json()
    return result
}

function availableTestCase(run: Run, instance: runInstance): testInstance {
    let data: any = setupInstance()
    data.platform = {
        ...data.platform,
        ...run.system,
    }
    data.ciBuildId = run.buildID
    data.machineId = instance.machineId
    data.groupId = instance.groupId
    const req = http.request(
        'POST',
        `http://localhost:1234/runs/${instance.runId}/instances`,
        JSON.stringify(data),
        {
            headers: {
                'Content-Type': 'application/json',
                'x-cypress-version': '6.7.1',
            },
        },
    )
    const result: any = req.json()
    return result
}

function reportTestCaseStatus(initDate: Date, finishDate: Date, testInstance: testInstance) {
    const res = http.get('http://localhost:6565/v1/groups')
    let dataGroups: any = res.json('data')
    let data: any = reportInstance()

    for (let index = 0; index < dataGroups.length; index++) {
        const group: any = dataGroups[index]
        if (
            group.attributes.name.trim() !== '' &&
            group.attributes.path.indexOf(testInstance.spec) > -1 &&
            group.attributes.checks &&
            group.attributes.checks.length > 0
        ) {
            data.stats.tests++
            let isFail = false
            for (let y = 0; y < group.attributes.checks.length; y++) {
                const check: any = group.attributes.checks[y]
                if (check.fails) {
                    isFail = true
                    y = group.attributes.checks.length
                }
            }
            if (!isFail) {
                data.stats.passes++
            } else {
                data.stats.failures++
            }

            data.tests.push({
                clientId: 'r' + index,
                testId: 'r' + index,
                title: [group.attributes.name],
                state: !isFail ? 'passed' : 'failed',
                body: '',
                displayError: null,
                attempts: [
                    {
                        state: !isFail ? 'passed' : 'failed',
                        error: null,
                        timings: {
                            lifecycle: 0,
                            'before all': [],
                            'before each': [
                                {
                                    hookId: 'h' + index,
                                    fnDuration: 0,
                                    afterFnDuration: 0,
                                },
                            ],
                            test: {
                                fnDuration: 0,
                                afterFnDuration: 0,
                            },
                            'after each': [],
                        },
                        failedFromHookId: null,
                        wallClockStartedAt: initDate.toJSON(),
                        wallClockDuration: finishDate.getTime() - initDate.getTime(),
                        videoTimestamp: null,
                    },
                ],
            })
        }
    }

    if (data.stats.tests > 0) {
        data.stats.suites = 1
        data.reporterStats.suites = data.stats.suites
        data.reporterStats.tests = data.stats.tests
        data.reporterStats.passes = data.stats.passes
        data.reporterStats.pending = data.stats.pending
        data.reporterStats.failures = data.stats.failures
        data.reporterStats.start = data.stats.wallClockStartedAt = initDate.toJSON()
        data.reporterStats.end = data.stats.wallClockEndedAt = finishDate.toJSON()
        data.reporterStats.duration = data.stats.wallClockDuration =
            finishDate.getTime() - initDate.getTime()
    }

    const req = http.request(
        'POST',
        `http://localhost:1234/instances/${testInstance.instanceId}/tests`,
        JSON.stringify(data),
        {
            headers: {
                'Content-Type': 'application/json',
                'x-cypress-version': '6.7.1',
            },
        },
    )

    log(`Body: http://localhost:1234/instances/${testInstance.instanceId}/tests`)

    const req2 = http.request(
        'POST',
        `http://localhost:1234/instances/${testInstance.instanceId}/results`,
        JSON.stringify(data),
        {
            headers: {
                'Content-Type': 'application/json',
                'x-cypress-version': '6.7.1',
            },
        },
    )

    const result: any = req2.json()
    return result
}

export function run(run: Run) {
    const instance = createRun(run)
    for (1; 1 === 1; 1) {
        const testCase = availableTestCase(run, instance)
        if (testCase.spec != null) {
            const initTime = new Date()
            run.test(testCase.spec)
            const finishTime = new Date()
            sleep(1)
            const data = reportTestCaseStatus(initTime, finishTime, testCase)
            log(JSON.stringify(data))
        } else {
            break
        }
    }
}
