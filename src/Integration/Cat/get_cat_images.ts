import { expect } from 'chai'
import { group } from 'k6'
import { assrt, CC_api, clearCookies, log } from '../../common/general'

interface DataSetup {
    limitPagination: number
}

export function setup() {
    return {
        limitPagination: 5
    }
}

export default (data: DataSetup) => {
    group('GET /', () => {

        group('Should match element size', function () {
            const limitPagination = data.limitPagination
            let res = CC_api(
                'GET',
                `https://api.thecatapi.com/v1/images/search?limit=${limitPagination}&page=1`,
                {},
                {},
            )
            assrt('Status api request', () => expect(res.status).to.eq(200))
            assrt('Verify length pagination', () => expect(res.json('#')).to.eq(limitPagination))
            assrt('Verify length not unless that expect', () =>
                expect(res.json('#')).to.not.lessThan(limitPagination),
            )
        })
    })
}

export function teardown(data: DataSetup) {
    log(`Finish validations`)
    clearCookies()
}
