import { expect } from 'chai'
import faker from 'faker/locale/en_US'
import { group } from 'k6'
import { assrt, CC_api, clearCookies, log } from '../../common/general'

interface DataSetup {
    catAPIName: string
    catName: string
    catWikiName: string
    catRamdomName: string
}

export function setup() {
    const catAPIName = 'The Cat API'
    const catName = 'Abyssinian'
    const catWikiName = 'Abyssinian cat'
    const ramdomName = faker.name.findName()
    const catRamdomName = ramdomName + ' ' + faker.random.word('{cat}')
    return {
        catAPIName,
        catRamdomName,
        catName,
        catWikiName,
    }
}

export default (data: DataSetup) => {
    group('GET /', () => {
        group('Should match API name', function () {
            const res = CC_api('GET', 'https://api.thecatapi.com/', {}, {})
            assrt('Status', () => expect(res.status).to.eq(200))
            assrt('Match API Name', () => expect(res.json('message')).to.eq(data.catAPIName))
        })

        group('Should match search cat by name', function () {
            let res = CC_api(
                'GET',
                `https://api.thecatapi.com/v1/breeds/search?q=${data.catName}`,
                {},
                {},
            )
            assrt('Status', () => expect(res.status).to.eq(200))
            assrt('Match API Name', () => expect(res.json('0.name')).to.eq(data.catName))
        })

        group('Should match search cat by name and verify wiki title', function () {
            let res = CC_api(
                'GET',
                `https://api.thecatapi.com/v1/breeds/search?q=${data.catName}`,
                {},
                {},
            )
            assrt('Status api request', () => expect(res.status).to.eq(200))
            const wiki_uri: any = res.json('0.wikipedia_url')
            res = CC_api('GET', wiki_uri, {}, {})
            assrt('Status wiki request', () => expect(res.status).to.eq(200))
            assrt('Match API Name', () =>
                expect(res.html('#firstHeading').text()).to.eq(data.catWikiName),
            )
        })

        group('Should not found cats with random name', function () {
            let res = CC_api(
                'GET',
                `https://api.thecatapi.com/v1/breeds/search?q=${escape(data.catRamdomName)}`,
                {},
                {},
            )
            assrt('Status api request', () => expect(res.status).to.eq(200))
            assrt('Verify length search', () => expect(res.json('#')).to.eq(0))
        })
    })
}

export function teardown(data: DataSetup) {
    log(`Finish validations`)
    clearCookies()
}
