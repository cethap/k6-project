export function setupDashboard() {
    return {
        ci: {
            params: null,
            provider: null,
        },
        specs: [],
        commit: {
            sha: '...',
            branch: '...',
            authorName: '...',
            authorEmail: '...',
            message: '...',
            remoteOrigin: 'git@gitlab.com:project.git',
            defaultBranch: null,
        },
        group: null,
        platform: {
            browserName: 'k6',
            browserVersion: 'v0.30.0',
        },
        parallel: true,
        ciBuildId: '...',
        projectId: __ENV.PROJECT_SCY || 'b6t8az',
        recordKey: 'xxx',
        specPattern: '...',
        tags: [''],
        runnerCapabilities: { dynamicSpecsInSerialMode: true, skipSpecAction: true },
    }
}

export function setupInstance() {
    return {
        spec: null,
        groupId: '...',
        machineId: '...',
        platform: {
            browserName: 'k6',
            browserVersion: 'v0.30.0',
        },
    }
}

export function reportInstance() {
    return {
        stats: {
            suites: 0,
            tests: 0,
            passes: 0,
            pending: 0,
            skipped: 0,
            failures: 0,
            wallClockStartedAt: '...',
            wallClockEndedAt: '...',
            wallClockDuration: 0,
        },
        tests: [],
        error: null,
        video: false,
        hooks: [],
        screenshots: [],
        config: {
            video: false,
            videoCompression: 32,
            videoUploadOnPasses: true,
            videosFolder: './videos',
        },
        reporterStats: {
            suites: 0,
            tests: 0,
            passes: 0,
            pending: 0,
            failures: 0,
            start: '...',
            end: '...',
            duration: 0,
        },
    }
}
