import http, { RefinedParams, ResponseType, RequestBody, RefinedResponse } from 'k6/http'
import { check } from 'k6'

export function log(msg: any): void {
    console.log(msg, '{[VU]}_' + __VU)
}

export function assrt(message: string, assert: any) {
    let target: any = {}
    try {
        assert()
        target[message] = () => true
        check(0, target)
    } catch (error) {
        target[message + ' => ' + error] = () => false
        check(0, target)
    }
}

export function clearCookies() {
    const BASE_URL = __ENV.BASE_URL
    const jars = http.cookieJar().cookiesForURL(BASE_URL)
    let jar = http.cookieJar()
    Object.keys(jars).forEach((key) => {
        jar.set(BASE_URL, key, '')
    })
}

export function CC_api(
    type: string,
    url: string,
    body: RequestBody,
    opts: RefinedParams<ResponseType>,
): RefinedResponse<ResponseType> {
    const BASE_URL = __ENV.BASE_URL || ''
    return http.request(type, BASE_URL + url, body, opts)
}

/**
 * @example //get test's name for data creation.
 * CC_get_parent_title()
 * Archivo: ../../common/general
 * Devuelve el nombre de la prueba, valido para efectos de debug de scripts.
 */
export function CC_get_parent_title() {
    const res = http.get('http://localhost:6565/v1/groups')
    if (!__ENV['current_test']) {
        return 'EmptyPath'
    } else {
        const data: any = res.json('data')
        let current_step: any = 'EmptyPath'
        for (const step of data) {
            const pathParts = step.attributes.path.split('::')
            if (pathParts.length > 1) {
                if (__ENV['current_test'] == pathParts[1]) {
                    current_step = step.attributes.name
                }
            }
        }
        return current_step
    }
}
