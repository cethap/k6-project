const path = require('path')
const fs = require('fs')

let entries = './test.ts'

module.exports = {
    mode: 'production',
    context: path.join(__dirname, 'src'),
    entry: entries,
    output: {
        path: path.join(__dirname, 'dist'),
        libraryTarget: 'commonjs',
        filename: '[name].js',
    },
    resolve: {
        extensions: ['.ts', '.js'],
    },
    module: {
        rules: [
            {
                test: /\.ts$/,
                use: 'babel-loader',
            },
        ],
    },
    target: 'web',
    externals: /^k6(\/.*)?/,
    stats: {
        colors: true,
        warnings: false,
    },
}
