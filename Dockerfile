FROM loadimpact/k6 as k6

FROM node:16-alpine
RUN mkdir /automation
WORKDIR /automation
RUN mkdir report
COPY --from=k6 /usr/bin/k6 /usr/bin/
COPY . .
RUN rm -rf .git
RUN npm install
RUN npm run build

ENTRYPOINT [ "npm", "run", "k6" ]
