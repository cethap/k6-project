const fs = require('fs')
const builder = require('junit-report-builder');
let testCasesResults = []
const getTestCases = (groups) => {
    for (const group in groups) {
        if(groups[group]){
            if(getTestChecks(groups[group].checks) == 0){
                getTestCases(groups[group].groups)
            }else{
                testCasesResults.push({
                    name: group,
                    checks: getTestChecks(groups[group].checks)
                })
            }
        }
    }
}

const getTestChecks = (checks) => {
    let status = 0
    for (const check in checks) {
        if(checks[check].passes){
            status++
        }
        if(checks[check].fails){
            status = -1
            break
        }
    }
    return status
}


fs.readFile('test.json', 'utf8' , (err, data) => {
  if (err) {
    console.error(err)
    return
  }
  const metrics = JSON.parse(data)
  const root_groups = metrics.root_group.groups
  testCasesResults = []
  getTestCases(root_groups)

    // Create a test suite
    var suite = builder.testSuite().name('Integration Test');

    for (const result of testCasesResults) {
        if(result.checks > 0){
            suite.testCase()
                .name(result.name)
        }else if(result.checks == -1){
            suite.testCase()
                .name(result.name)
                .failure()
        }
    }
    builder.writeTo('junit_report/test-report.xml');
})